import { Component } from '@angular/core';
import { User } from '../../Core/Interfaces/User.interface';
import { UserService } from '../../Core/Services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent {
  user!: User;

  constructor(private userServ: UserService) {}
  sub = this.userServ.User$.subscribe((user) => (this.user = user));
}
