import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../Core/Services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnDestroy {
  authState: any = false;

  constructor(private authServic: AuthService) {}
  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  sub = this.authServic.authState$.subscribe(
    (state: any) => (this.authState = state),
  );

  logout() {
    this.authServic.logout();
  }
}
