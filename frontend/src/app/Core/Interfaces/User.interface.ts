export interface User {
  email: string;
  id: 27;
  name: string;
  role: string;
  username: string;
}
