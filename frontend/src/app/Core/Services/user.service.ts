import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../Interfaces/User.interface';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user!: User;
  userBehaviorSubject$ = new BehaviorSubject<User>(this.user);
  User$ = this.userBehaviorSubject$.asObservable();

  // constructor() {}

  setUserData(user: User) {
    this.userBehaviorSubject$.next(user);
  }
}
