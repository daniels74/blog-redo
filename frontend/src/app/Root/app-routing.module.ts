import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../Features/home/home.component';
import { LoginComponent } from '../Features/login/login.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('../Features/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('../Features/login/login.module').then((m) => m.LoginModule),
  },
  {
    path: 'user/:id',
    loadChildren: () =>
      import('../Features/user/user.module').then((m) => m.UserModule),
  },
  // { path: 'a', loadChildren: () => import('./modulea/modulea.module').then(m => m.ModuleaModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
